﻿
using System;
using System.Collections.Generic;
using System.Linq;
using DefaultNamespace;
using UnityEngine;
using Random = UnityEngine.Random;
using Vector3 = System.Numerics.Vector3;

public class Recipe
{
    public Vector3 TargetConconction;

    public List<StatusEffect> RequiredStatues = new List<StatusEffect>();
    
    public List<StatusEffect> ForbiddenStatuses = new List<StatusEffect>();

    public Recipe()
    {
        // Generate a new random recipe
        
        TargetConconction = new Vector3(Random.Range(0, 255), Random.Range(0, 255), Random.Range(0, 255));
        
        var targetCount = Random.Range(0, 5);
        var enumValues = Enum.GetValues(typeof(StatusEffect)).Cast<StatusEffect>().ToList();

        for (var i = 0; i < targetCount; i++)
        {
            var index = Random.Range(0, enumValues.Count - 1);
            RequiredStatues.Add(enumValues[index]);
            enumValues.RemoveAt(index);
        }

        targetCount = Random.Range(0, 5);
        for (var i = 0; i < targetCount; i++)
        {
            var index = Random.Range(0, enumValues.Count - 1);
            ForbiddenStatuses.Add(enumValues[index]);
            enumValues.RemoveAt(index);
        }
        
        Debug.Log($"Generated a new recipe -- TargetConcoction: {TargetConconction}, RequiredStatuses: {RequiredStatues}, ForbiddenStatuses: {ForbiddenStatuses}");
    }
}
