﻿using System.Collections.Generic;

namespace DefaultNamespace
{
    public enum StatusEffect
    {
        None, 
        Fire, 
        Water, 
        Air, 
        Earth, 
        Cold, 
        Void, 
        Light, 
        Decay, 
        Growth, 
        Despair, 
        Hope, 
        Bravery, 
        Cowardice, 
        Pain, 
        Pleasure, 
        Beauty, 
        Horror, 
        Cosmos, 
        Time, 
        Gossip, 
        Whispers, 
        Envy, 
        Rage, 
        Greed, 
        Lust,
        Wrath,
        Sloth,
        Nullified,
        Divine,
        Demonic,
    }

    public static class StatusMappings
    {
        // For every pair, the effects are cancelled for both
        public static List<(StatusEffect, StatusEffect)> MutexStatuses = new List<(StatusEffect, StatusEffect)>()
        {
            (StatusEffect.Fire, StatusEffect.Water),
            (StatusEffect.Void, StatusEffect.Light),
            (StatusEffect.Decay, StatusEffect.Growth),
            (StatusEffect.Despair, StatusEffect.Hope),
            (StatusEffect.Bravery, StatusEffect.Cowardice),
            (StatusEffect.Demonic, StatusEffect.Divine),
            
        };
        
        // Duplicates of each provide a multiplier to their effect
        public static List<StatusEffect> AmplifierStatuses = new List<StatusEffect>()
        {
            StatusEffect.Fire,
            StatusEffect.Despair,
            StatusEffect.Horror,
            StatusEffect.Light,
            StatusEffect.Gossip,
            StatusEffect.Rage
        };
        
        // Status effects that increase instability
        public static List<StatusEffect> Unstable = new List<StatusEffect>()
        {
            StatusEffect.Fire,
            StatusEffect.Void,
            StatusEffect.Growth,
            StatusEffect.Pain,
            StatusEffect.Envy,
            StatusEffect.Greed,
            StatusEffect.Despair,
            StatusEffect.Horror,
            StatusEffect.Gossip
        };
        
        // Status effects that reduce instability
        public static List<StatusEffect> Stable = new List<StatusEffect>()
        {
            StatusEffect.Earth,
            StatusEffect.Water,
            StatusEffect.Pleasure,
            StatusEffect.Bravery,
            StatusEffect.Light
        };
        
        // Status effects that increase potency
        public static List<StatusEffect> Potent = new List<StatusEffect>()
        {
            StatusEffect.Time,
            StatusEffect.Rage,
            StatusEffect.Lust
        };
        
        // Status effects that will wipe all status effects out
        public static List<StatusEffect> Nullifier = new List<StatusEffect>()
        {
            StatusEffect.Nullified
        };

        public static List<List<StatusEffect>> GameOverSets = new List<List<StatusEffect>>()
        {
            new List<StatusEffect>() { StatusEffect.Greed, StatusEffect.Lust, StatusEffect.Wrath, StatusEffect.Sloth },
            new List<StatusEffect>() { StatusEffect.Time, StatusEffect.Cosmos, StatusEffect.Void, StatusEffect.Horror }
        };
    }
}