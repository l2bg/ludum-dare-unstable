using System;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class IngredientSpawner : MonoBehaviour
{
    public List<Sprite> IngredientSprites;

    public GameObject IngredientPrefab;
    
    [SerializeField] 
    public Image InteractPrompt;

    [SerializeField] 
    public GameObject PopupImage;
    
    private string[] _statusNames = Enum.GetNames(typeof(StatusEffect));

    private Dictionary<StatusEffect, List<string>> Adjectives;
    private Dictionary<StatusEffect, List<string>> Noun;
    
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.DrawIcon(transform.position, "Ingredients\\cauldron.png", true);
    }
#endif
    
    private void Awake()
    {
        var ingredientObject = Instantiate(IngredientPrefab, transform.position, Quaternion.identity, transform);
        ingredientObject.layer = LayerMask.NameToLayer("Items");
        var ingredient = ingredientObject.GetComponent<Ingredient>();

        var randomValues = new List<int>()
        {
            0, 0, 0, 0, 0, 0
        };

        var numValues = Random.Range(1, 4);

        for (var i = 0; i < numValues; i++)
        {
            randomValues[Random.Range(0, 5)] = Random.Range(1, 255);
        }
        
        var additiveColor = new Vector3(randomValues[0], randomValues[1], randomValues[2]);
        var subtractiveColor = new Vector3(randomValues[3], randomValues[4], randomValues[5]);

        var statusEffect = (StatusEffect) Enum.Parse(typeof(StatusEffect), _statusNames[Random.Range(0, _statusNames.Length -1)]);
        
        var name = GeneratedNames.Adjectives[statusEffect][Random.Range(0, GeneratedNames.Adjectives[statusEffect].Count - 1)] + "\n" + GeneratedNames.Nouns[Random.Range(0, GeneratedNames.Nouns.Count - 1)];
        var label = "";
        
        ingredient.Setup(
            name,
            label, 
            IngredientSprites[Random.Range(0, IngredientSprites.Count - 1)],
            additiveColor,
            subtractiveColor,
            statusEffect,
            InteractPrompt,
            PopupImage);
    }
}
