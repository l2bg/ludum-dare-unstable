﻿
using System.Collections.Generic;
using DefaultNamespace;

public static class GeneratedNames
{
    public static Dictionary<StatusEffect, List<string>> Adjectives = new Dictionary<StatusEffect, List<string>>()
    {
        { 
            StatusEffect.None, 
            new List<string>()
            {
                "Inert",
                "Boring",
                "Sticky"
            }
        },
        { 
            StatusEffect.Air, 
            new List<string>()
            {
                "Fluffy",
                "Aerie",
                "Weightless"
            }
        },
        { 
            StatusEffect.Beauty, 
            new List<string>()
            {
                "Beautiful",
                "Handsome",
                "Vane"
            }
        },
        { 
            StatusEffect.Bravery, 
            new List<string>()
            {
                "Stout",
                "Stoic",
                "Unyielding"
            }
        },
        { 
            StatusEffect.Cold, 
            new List<string>()
            {
                "Frigid",
                "Frosty",
                "Clammmy"
            }
        },
        { 
            StatusEffect.Cosmos, 
            new List<string>()
            {
                "Cosmic",
                "Expansive",
                "Starry"
            }
        },
        { 
            StatusEffect.Cowardice, 
            new List<string>()
            {
                "Quivering",
                "Cowardly",
                "Timid"
            }
        },
        { 
            StatusEffect.Decay, 
            new List<string>()
            {
                "Oozing",
                "Moldy",
                "Desecrated"
            }
        },
        { 
            StatusEffect.Demonic, 
            new List<string>()
            {
                "Unholy",
                "Abyssal",
                "Infernal"
            }
        },
        { 
            StatusEffect.Despair, 
            new List<string>()
            {
                "Hopeless",
                "Woe-inducing",
                "Tearful"
            }
        },
        { 
            StatusEffect.Divine, 
            new List<string>()
            {
                "Awesome",
                "Divine",
                "Blessed"
            }
        },
        { 
            StatusEffect.Earth, 
            new List<string>()
            {
                "Dirty",
                "Stony",
                "Muddy"
            }
        },
        { 
            StatusEffect.Envy, 
            new List<string>()
            {
                "Green",
                "Jealous",
                "Spiteful"
            }
        },
        { 
            StatusEffect.Fire, 
            new List<string>()
            {
                "Very Hot",
                "Flaming",
                "Molten"
            }
        },
        { 
            StatusEffect.Gossip, 
            new List<string>()
            {
                "Ugly",
                "Secretive",
                "Liar's"
            }
        },
        { 
            StatusEffect.Greed, 
            new List<string>()
            {
                "Highly Sought",
                "Covetous",
                "Hoarder's"
            }
        },
        { 
            StatusEffect.Growth, 
            new List<string>()
            {
                "Verdant",
                "Luscious",
                "Thick"
            }
        },
        { 
            StatusEffect.Hope, 
            new List<string>()
            {
                "Glorious",
                "Emboldening",
                "Baited"
            }
        },
        { 
            StatusEffect.Horror, 
            new List<string>()
            {
                "Frightful",
                "Hideous",
                "Panicked"
            }
        },
        { 
            StatusEffect.Light, 
            new List<string>()
            {
                "Light",
                "Bright",
                "Glorious"
            }
        },
        { 
            StatusEffect.Lust, 
            new List<string>()
            {
                "Passionate",
                "Erotic",
                "Savory"
            }
        },
        { 
            StatusEffect.Nullified, 
            new List<string>()
            {
                "Nullifying"
            }
        },
        { 
            StatusEffect.Pain, 
            new List<string>()
            {
                "Heartbroken",
                "Wounded",
                "Tramatic"
            }
        },
        { 
            StatusEffect.Pleasure, 
            new List<string>()
            {
                "Delightful",
                "Wonderful",
                "Wholesome"
            }
        },
        { 
            StatusEffect.Rage, 
            new List<string>()
            {
                "Bitter",
                "Furious",
                "Angsty"
            }
        },
        { 
            StatusEffect.Sloth, 
            new List<string>()
            {
                "Lazy",
                "Sleepy",
                "Procrastinator's"
            }
        },
        { 
            StatusEffect.Time, 
            new List<string>()
            {
                "Prompt",
                "Ancient",
                "Modern"
            }
        },
        { 
            StatusEffect.Void, 
            new List<string>()
            {
                "Empty",
            }
        },
        { 
            StatusEffect.Water, 
            new List<string>()
            {
                "Wet",
                "Oily",
                "Deep"
            }
        },
        { 
            StatusEffect.Whispers, 
            new List<string>()
            {
                "Spooky",
                "Barely Visible",
                "Secretive"
            }
        },
        { 
            StatusEffect.Wrath, 
            new List<string>()
            {
                "Vengeful",
                "Righteous",
                "Vindictive"
            }
        }
    };


    public static List<string> Nouns = new List<string>()
    {
        "Tincture",
        "Powder",
        "Shavings",
        "Balm",
        "Oil",
        "Salt",
        "Clippings",
        "Wafer"
    };
}
