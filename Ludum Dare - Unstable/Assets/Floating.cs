using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floating : MonoBehaviour
{
    [SerializeField] float catchupSpeed = 10f;
    [SerializeField] float floatBounceSpeed = 1;
    [SerializeField] float floatBounceMagnitude = 0.1f;

    Vector2 initialLocation;

    private void OnEnable()
    {
        initialLocation = transform.position;
    }

    void Update()
    {
        float floatOffset = Mathf.Sin(Time.time * floatBounceSpeed);
        Vector2 offsetActual = new Vector2(0, floatOffset * floatBounceMagnitude);
        Vector2 dest = initialLocation + offsetActual;
        transform.position = Vector2.Lerp(transform.position, dest, catchupSpeed * Time.deltaTime);

    }
}
