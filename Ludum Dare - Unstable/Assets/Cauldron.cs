﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using Random = UnityEngine.Random;

public class Cauldron : Item
{
    public ParticleSystem DropFX;
    public ParticleSystem ExplosionFX;
    public ParticleSystem InstabilityFX;
    public float maxShake = 0.02f;
    public float maxInstabilityFXSpeed = 10f;
    public float maxInstabilityFXLifetime = 1f;
    public float maxInstabilityEmissionRate = 200f;

    public List<AudioClip> SplashSounds;
    public AudioClip ExplosionSound;

    public SpriteRenderer Liquid;

    public GameObject SplashPrefab;
    public GameObject ExplosionPrefab;

    public float InstabilityGainPerTick = 0.1f;
    
    public float Instability = 0;

    // TODO this is different than ingredient potency, which is confusing I know
    public float Potency = 100;

    public int TotalIngredientCount;
    
    public Vector3 Concoction;
    
    public List<StatusEffect> StatusEffects = new List<StatusEffect>();

    public float successThreshold = 20.0f;
    
    private Light2D _Light2d;
    private Vector3 _initial_pos;
    private GameState _gameState;

    private AudioSource _audioSource;
    private bool _exploded = false;

    public override bool IsGrabbable => false;
    
    private void Awake()
    {
        _initial_pos = transform.position;
        _gameState = GameObject.FindWithTag("GameController")?.GetComponent<GameState>();

        if (_gameState == null)
        {
            throw new UnityException("Required to have a GameState in your scene");
        }

        _Light2d = GetComponentInChildren<Light2D>();
        _audioSource = GetComponent<AudioSource>();
    }

    private void FixedUpdate()
    {
        if (_gameState.isGameOver)
        {
            return;
        }
        
        Color color = new Color(Concoction.x / 255f, Concoction.y / 255f, Concoction.z / 255f);

        if (TotalIngredientCount > 0 && !_exploded)
        {
            var main = InstabilityFX.main;
            main.startSpeed = new ParticleSystem.MinMaxCurve(4, Mathf.Lerp(4, maxInstabilityFXSpeed, Instability / 100));
            main.startColor = color;
            main.startLifetime = new ParticleSystem.MinMaxCurve(0.5f, Mathf.Lerp(0.5f, maxInstabilityFXLifetime, Instability / 100));
            var emission = InstabilityFX.emission;
            emission.rateOverTime = new ParticleSystem.MinMaxCurve(15f, Mathf.Lerp(15, maxInstabilityEmissionRate, Instability / 100));

            if (!InstabilityFX.isPlaying)
            {
                InstabilityFX.Play();
            }

            float maxshake = Mathf.Lerp(0, maxShake, Instability / 100);
            Vector3 shakeOffset = new Vector3(Random.Range(-maxshake, maxshake), Random.Range(-maxshake, maxshake));
            transform.position = _initial_pos + shakeOffset;

            Liquid.color = color;
        }
        else
        {
            Liquid.color = Color.black;
        }

        _Light2d.color = color;
        _Light2d.intensity = 1; // TODO what is potency supposed to do?

        Instability += InstabilityGainPerTick * TotalIngredientCount;
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.layer != LayerMask.NameToLayer("Items"))
        {
            return;
        }
        
        ApplyIngredient(other.gameObject.GetComponent<Ingredient>());
    }

    private void ApplyIngredient(Ingredient ingredient)
    {
        Debug.Log($"Applying ingredient to potion: {ingredient.ItemName}");

        DropFX.Play();
        Instantiate(SplashPrefab, transform.position, Quaternion.identity);

        // TODO: Status effect application could influence application of composition?
        ApplyStatusEffect(ingredient, out var multiplier);
        
        AddToConcoction(ingredient, multiplier);

        RemoveFromConcoction(ingredient);

        TotalIngredientCount++;

        if (StatusMappings.Potent.Contains(ingredient.StatusEffect))
        {
            Potency += TotalIngredientCount * TotalIngredientCount;
        }
        else
        {
            Potency -= TotalIngredientCount * TotalIngredientCount;
        }

        _audioSource.PlayOneShot(SplashSounds[Random.Range(0, SplashSounds.Count - 1)]);
        
        Destroy(ingredient.gameObject);
    }

    private void ApplyStatusEffect(Ingredient ingredient, out float multiplier)
    {
        multiplier = 1f;

        StatusEffects.Add(ingredient.StatusEffect);
        
        // Apply Nullifiers
        if (StatusMappings.Nullifier.Contains(ingredient.StatusEffect))
        {
            StatusEffects = new List<StatusEffect>();
        }

        foreach (var gameOverSet in StatusMappings.GameOverSets)
        {
            if (!gameOverSet.Except(StatusEffects).Any())
            {
                Instability = 1000;
                EvaluateConcoctionStability();
                return;
            }
        }
        
        // Look for mutually exlusive statuses
        foreach (var mutex in StatusMappings.MutexStatuses)
        {
            // Mutex set not applicable
            if (mutex.Item1 != ingredient.StatusEffect 
                && mutex.Item2 != ingredient.StatusEffect)
            {
                continue;
            }

            if (mutex.Item1 == ingredient.StatusEffect && StatusEffects.Contains(mutex.Item2) 
                || mutex.Item2 == ingredient.StatusEffect && StatusEffects.Contains(mutex.Item1))
            {
                StatusEffects.Remove(mutex.Item1);
                StatusEffects.Remove(mutex.Item2);
            }
        }

        var amplificationMultiplier = 1;
        if (StatusMappings.AmplifierStatuses.Contains(ingredient.StatusEffect))
        {
            amplificationMultiplier = StatusEffects.FindAll(se => se == ingredient.StatusEffect).Count;
        }
        
        // Apply instability first
        if (StatusMappings.Unstable.Contains(ingredient.StatusEffect))
        {
            Instability += 10 * amplificationMultiplier;
        }
        
        // Apply stabalizers
        if (StatusMappings.Stable.Contains(ingredient.StatusEffect))
        {
            Instability -= 5 * amplificationMultiplier;
        }
        
        // Apply potency
        if (StatusMappings.Potent.Contains(ingredient.StatusEffect))
        {
            multiplier = (float) (1.5 * amplificationMultiplier);
        }
        
        EvaluateConcoctionStability();
    }
    
    private void AddToConcoction(Ingredient ingredient, float multiplier = 0f)
    {
        Concoction.x += ingredient.AdditiveColor.x * multiplier;
        Concoction.y += ingredient.AdditiveColor.y * multiplier;
        Concoction.z += ingredient.AdditiveColor.z * multiplier;

        EvaluateConcoctionStability();
    }

    private void RemoveFromConcoction(Ingredient ingredient, float multiplier = 0f)
    {
        Concoction.x -= ingredient.SubtractiveColor.x * multiplier;
        Concoction.y -= ingredient.SubtractiveColor.y * multiplier;
        Concoction.z -= ingredient.SubtractiveColor.z * multiplier;
        
        EvaluateConcoctionStability();
    }
    
    private void EvaluateConcoctionStability()
    {
        // clamping this down to a min 0
        if (Concoction.x < 0) Concoction.x = 0;
        if (Concoction.y < 0) Concoction.y = 0;
        if (Concoction.z < 0) Concoction.z = 0;
        
        // Values over 255 are converted to instability
        // If over 255, bump instability by the difference then clamp back down to 255
        if (Concoction.x > 255)
        {
            Instability += (int) Concoction.x - 255;
            Concoction.x = 255;
        }
        
        if (Concoction.y > 255)
        {
            Instability += (int) Concoction.y - 255;
            Concoction.y = 255;
        }
        
        if (Concoction.z > 255)
        {
            Instability += (int) Concoction.z - 255;
            Concoction.z = 255;
        }

        if (Instability > 100)
        {
            // if the potion becomes unstable....
            // Game over!
            _gameState.EndGame();
        }
    }

    public override void OnInteract(Grubber grubber)
    {
        Debug.Log($"Sampling the wares: {TotalIngredientCount}");
        
        if (TotalIngredientCount > 0)
        {
            // TODO confirm prompt?
            // TODO friendly warning?

            StartCoroutine(SampleTheWares());
        }
    }

    private IEnumerator SampleTheWares()
    {
        var Player = GameObject.FindWithTag("Player").GetComponent<PlayerCharacterController>();

        yield return Player.Drink();

        EvaluateFinalProduct();
    }

    public void EvaluateFinalProduct()
    {
        Debug.Log("Evaluating the final product");
        
        var recipe = _gameState.CurrentRecipe;

        if (Math.Abs(recipe.TargetConconction.X - Concoction.x) <= successThreshold
            && Math.Abs(recipe.TargetConconction.Y - Concoction.y) <= successThreshold
            && Math.Abs(recipe.TargetConconction.Z - Concoction.z) <= successThreshold)
        {
            Debug.Log("Success");
            // TODO success!
            // TODO tag evaluation
            _gameState.EndGame(true);
            return;
        }
        
        // Not quite there
        Debug.Log("Ew, failure");
        _gameState.NotThereYet();

    }

    public IEnumerator BlowUp(Action callback)
    {
        _audioSource.PlayOneShot(ExplosionSound);
        
        yield return new WaitForSeconds(0.5f);

        _exploded = true;
        Instantiate(ExplosionPrefab, transform.position, Quaternion.identity);
        ExplosionFX.Play();
        InstabilityFX.Stop();
        
        yield return new WaitForSeconds(2);

        callback();
    }
    
    public override void OnDrop()
    {
        // not grabbable so noop
    }

    protected override void HandleTriggerEnter(Grubber grubber)
    {
        if (TotalIngredientCount > 0 && !grubber.IsGrabbing)
        {
            ShowPrompts(); 
        }
        
    }

    protected override void HandleTriggerExit(Grubber collision)
    {
        HidePrompts();
    }
}
