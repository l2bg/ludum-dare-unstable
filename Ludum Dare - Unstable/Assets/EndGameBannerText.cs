using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGameBannerText : MonoBehaviour
{
    private Text BannerText;
    
    public void SetEndGameBannerText(string text, Color color)
    {
        if (BannerText == null)
        {
            BannerText = GetComponent<Text>();
        }

        if (BannerText != null)
        {
            BannerText.text = text;
            BannerText.color = color;
        }
    }
}
