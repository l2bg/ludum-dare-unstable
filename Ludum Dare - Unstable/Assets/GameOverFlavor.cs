using UnityEngine;
using UnityEngine.UI;

public class GameOverFlavor : MonoBehaviour
{
    private Text FlavorText;

    public void SetFlavorText(string text)
    {
        if (FlavorText == null)
        {
            FlavorText = GetComponent<Text>();
        }

        if (FlavorText != null)
        {
            FlavorText.text = text;
        }
    }
}
