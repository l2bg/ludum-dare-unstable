using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class CandleFlicker : MonoBehaviour
{
    [SerializeField] float minRadius = 2f;
    [SerializeField]  float maxRadius = 3f;
    [SerializeField]  float flickerDelay = 0.1f;

    Light2D[] lights;
    float flickerTimer = 0;

    // Start is called before the first frame update
    void Start()
    {
        lights = GetComponentsInChildren<Light2D>();
    }

    // Update is called once per frame
    void Update()
    {
        flickerTimer -= Time.deltaTime;
        if (flickerTimer < 0)
        {
            flickerTimer = flickerDelay;
            foreach (Light2D light in lights)
            {
                light.pointLightOuterRadius = Mathf.Lerp(light.pointLightOuterRadius, Random.Range(minRadius, maxRadius), flickerDelay);
            }
        }
    }
}
