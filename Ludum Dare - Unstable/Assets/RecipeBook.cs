
using UnityEngine;
using UnityEngine.UI;

public class RecipeBook : Item
{
    public Image TargetColor;
    
    private Animator m_Animator;

    public override bool IsGrabbable => false;

    private GameState _gameState;
    
    // Start is called before the first frame update
    void Awake()
    {
        m_Animator = GetComponent<Animator>();
        _gameState = GameObject.FindWithTag("GameController")?.GetComponent<GameState>();

        if (_gameState == null)
        {
            throw new UnityException("Required to have a GameState in your scene");
        }
    }

    protected override void ShowPrompts()
    {
        base.ShowPrompts();
        m_Animator.SetBool("isOpen", true);
    }

    protected override void HidePopups()
    {
        base.HidePopups();
        m_Animator.SetBool("isOpen", false);
    }

    protected override void HandleTriggerEnter(Grubber grubber)
    {
        ShowPrompts();
    }

    protected override void HandleTriggerExit(Grubber collision)
    {
        HidePrompts();
        HidePopups();
    }

    public override void OnInteract(Grubber grubber)
    {
        HidePrompts();
        ShowPopups();
        
        Debug.Log("Interacting with recipe book");

        var recipe = _gameState.CurrentRecipe;
        
        if (recipe == null)
        {
            return;
        }
        
        if (TargetColor != null)
        {
            TargetColor.color = new Color(recipe.TargetConconction.X / 255f, recipe.TargetConconction.Y / 255f, recipe.TargetConconction.Z / 255f);
        }
    }

    public override void OnDrop()
    {
        // can't be grabbed, so noop
    }
}
