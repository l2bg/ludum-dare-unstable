using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;
using Vector2 = UnityEngine.Vector2;
using Random = UnityEngine.Random;

public enum DirectionFacing
{
    Down, Up, Left, Right
}

public class PlayerCharacterController : MonoBehaviour
{
    public float MovementSpeed = 10.0f;
    public float HustleFactor = 2.0f;

    public Transform Arms;
    public Grubber Grubber;

    public float WalkInSpeed = 5f;
    public List<AudioClip> FootstepSounds;
    private AudioSource _audioSource;

    private bool _isRunning;
    private Vector2 _currentMovementVector;
    private Rigidbody2D _rigidBody;

    private Animator _animator;

    private Ingredient _leftHand;
    //private Ingredient _rightHand;

    private bool _isMoving;
    
    private GameState _gameState;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void Awake()
    {
        _gameState = GameObject.FindWithTag("GameController")?.GetComponent<GameState>();

        if (_gameState == null)
        {
            throw new UnityException("Required to have a GameState in your scene");
        }
        
        _rigidBody = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
    }

    public void EnableInput()
    {
        _gameState.PlayerInput.CharacterControls.Movement.started += OnPlayerMovement;
        _gameState.PlayerInput.CharacterControls.Movement.canceled += OnPlayerMovement;
        _gameState.PlayerInput.CharacterControls.Movement.performed += OnPlayerMovement;

        _gameState.PlayerInput.CharacterControls.Hustle.started += OnGetTheLeadOut;
        _gameState.PlayerInput.CharacterControls.Hustle.canceled += OnGetTheLeadOut;

        _gameState.PlayerInput.CharacterControls.LeftHandInteract.started += OnInteract;
        _gameState.PlayerInput.CharacterControls.LeftHandInteract.canceled += OnInteractReleased;
    }
    
    public void DisableInput()
    {
        _gameState.PlayerInput.CharacterControls.Movement.started -= OnPlayerMovement;
        _gameState.PlayerInput.CharacterControls.Movement.canceled -= OnPlayerMovement;
        _gameState.PlayerInput.CharacterControls.Movement.performed -= OnPlayerMovement;

        _gameState.PlayerInput.CharacterControls.Hustle.started -= OnGetTheLeadOut;
        _gameState.PlayerInput.CharacterControls.Hustle.canceled -= OnGetTheLeadOut;

        _gameState.PlayerInput.CharacterControls.LeftHandInteract.started -= OnInteract;
        _gameState.PlayerInput.CharacterControls.LeftHandInteract.canceled -= OnInteractReleased;
    }

    // walks the player into the scene from spawn
    public IEnumerator WalkToPoint(Vector3 destPosition, Action callback)
    {
        _animator.SetFloat("Vertical", -1);
        _animator.SetFloat("Speed", 1);

        var initialPosition = transform.position;
        
        float t = 0;
        while (transform.position != destPosition)
        {
            transform.position = Vector2.Lerp(initialPosition, destPosition, t);
            t += WalkInSpeed * Time.deltaTime;
            yield return null;
        }

        _animator.SetFloat("Speed", 0);

        callback();
    }

    public void PlayStep()
    {
        _audioSource.PlayOneShot(FootstepSounds[Random.Range(0, FootstepSounds.Count - 1)]);
    }

    private void FixedUpdate()
    {
        var currentSpeed = _isRunning ? MovementSpeed * HustleFactor : MovementSpeed;

        _rigidBody.velocity = _currentMovementVector * currentSpeed;

        if (!_isMoving)
        {
            return;
        }
        
        if (_currentMovementVector.x > 0)
        {
            Arms.transform.rotation = Quaternion.Euler(transform.position.x, Single.Epsilon, 90);
        }
        
        if (_currentMovementVector.x < 0)
        {
            Arms.transform.rotation = Quaternion.Euler(transform.position.x, Single.Epsilon, -90);
        }
        
        if (_currentMovementVector.y > 0)
        {
            Arms.transform.rotation = Quaternion.Euler(transform.position.x, Single.Epsilon, 180);
        }
        
        if (_currentMovementVector.y < 0)
        {
            Arms.transform.rotation = Quaternion.Euler(transform.position.x, Single.Epsilon, 0);
        }
    }

    void OnPlayerMovement(InputAction.CallbackContext context)
    {
        if (!_gameState.isGameStarted)
        {
            return;
        }
        
        var movement = context.ReadValue<Vector2>();

        _currentMovementVector.x = movement.x;
        _currentMovementVector.y = movement.y;
        
        _isMoving = math.abs(movement.x) > 0 || math.abs(movement.y) > 0;

        if (_isMoving)
        {
            _animator.SetFloat("Horizontal", movement.x);
            _animator.SetFloat("Vertical", movement.y);
            _animator.SetFloat("Speed", movement.sqrMagnitude);
        } else
        {
            _animator.SetFloat("Speed", 0);
        }
    }

    void OnGetTheLeadOut(InputAction.CallbackContext context)
    {
        if (!_gameState.isGameStarted)
        {
            return;
        }
        
        _isRunning = context.ReadValueAsButton();
    }
    
    void OnInteract(InputAction.CallbackContext context)
    {
        if (!_gameState.isGameStarted)
        {
            return;
        }

        Debug.Log("Interact button pressed");
        if (context.ReadValueAsButton())
        {
            Grubber.TryGrab();
        }
    }
    
    void OnInteractReleased(InputAction.CallbackContext context)
    {
        if (!_gameState.isGameStarted)
        {
            return;
        }
        
        Debug.Log("Interact button released");
        if (!context.ReadValueAsButton())
        {
            Grubber.TryDrop();
        }
    }

    public Coroutine Drink()
    {
        return StartCoroutine(TakeAFuckingSip());
    }

    private IEnumerator TakeAFuckingSip()
    {
        DisableInput();
        _animator.SetTrigger("Drink");

        yield return new WaitForSeconds(3);

        EnableInput();
    }

}
