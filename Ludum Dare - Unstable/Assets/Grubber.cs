﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Grubber : MonoBehaviour
{
    public Item grabbedIngredient;

    private BoxCollider2D _collider;

    public bool IsGrabbing;
    
    private void Awake()
    {
        _collider = GetComponent<BoxCollider2D>();
    }

    public void TryGrab()
    {
        Debug.Log("Grab called");
        
        if (grabbedIngredient != null)
        {
            Debug.Log("Already holding something");
            return;
        }
        
        var contactFilter = new ContactFilter2D
        {
            layerMask = LayerMask.GetMask("Items"),
            useLayerMask = true
        };

        var results = new List<Collider2D>();
        _collider.OverlapCollider(contactFilter, results);

        Debug.Log($"Found {results.Count} results");
        
        var item = results.FirstOrDefault()?.GetComponent<Item>();

        if (item == null)
        {
            Debug.Log("No Items found");
            return;
        }
        
        item.OnInteract(this);
            
        if (item.IsGrabbable)
        {
            grabbedIngredient = item;
            
            IsGrabbing = true;
            grabbedIngredient.GetComponent<Collider2D>().enabled = false;
            grabbedIngredient.transform.SetParent(transform);
            grabbedIngredient.transform.localPosition = Vector3.zero;
        }
    }

    public void TryDrop()
    {
        if (grabbedIngredient != null)
        {
            grabbedIngredient.OnDrop();
            IsGrabbing = false;
            grabbedIngredient.GetComponent<Collider2D>().enabled = true;
            grabbedIngredient.transform.SetParent(null);
            grabbedIngredient = null;
        }
    }
}
