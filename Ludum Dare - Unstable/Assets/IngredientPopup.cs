using UnityEngine;
using UnityEngine.UI;

public class IngredientPopup : MonoBehaviour
{
    public Image AdditiveSwatch;
    public Image SubtractiveSwatch;
    
    public void SetAdditiveSwatchColor(Color color)
    {
        AdditiveSwatch.color = color;
    }

    public void SetSubtractiveSwatchColor(Color color)
    {
        SubtractiveSwatch.color = color;
    }
}
