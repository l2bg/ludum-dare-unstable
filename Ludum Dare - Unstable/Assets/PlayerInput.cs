// GENERATED AUTOMATICALLY FROM 'Assets/PlayerInput.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerInput : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerInput()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerInput"",
    ""maps"": [
        {
            ""name"": ""CharacterControls"",
            ""id"": ""846b0bd3-a820-4469-ab98-3f533d871460"",
            ""actions"": [
                {
                    ""name"": ""Movement"",
                    ""type"": ""Value"",
                    ""id"": ""ff1c3a86-4b9a-4905-a710-2b91fcfe5023"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Hustle"",
                    ""type"": ""Button"",
                    ""id"": ""e5e2f91d-4b8f-4f31-a488-480603128c80"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""LeftHandInteract"",
                    ""type"": ""Button"",
                    ""id"": ""c826787d-f8f8-4a7d-9376-228f078ffa31"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RightHandInteract"",
                    ""type"": ""Button"",
                    ""id"": ""5ea1010d-203f-4fc2-9352-edc22ab084d8"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Hold""
                },
                {
                    ""name"": ""Escape"",
                    ""type"": ""Button"",
                    ""id"": ""a87097a1-0b91-4405-86ec-2cee0f0fde0a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Hold""
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""c6fe3b8d-baf4-4f52-82e1-6e5d199ef93f"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""d3417a5a-fb75-4d6b-972f-8bc7a706d8a9"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""620a1137-b7cb-4c4c-bb89-37a81dd2b548"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""bad8b813-aef7-4c4c-82c2-2796510f5f5c"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""b39730c1-35bb-4e84-b28a-bafcd2563169"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""592fbc1d-3762-458d-a465-83b7c7b3da28"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""71bdd122-2176-49c9-b17b-16e98696e5ee"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""c872b89c-2fbb-4e0c-99f7-474e77272b78"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""bdc6af41-ddef-4990-ab99-d3ebad93f5ee"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""9f0fed07-873c-4ed8-a3db-9f0ead54e8cf"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""c7d601e9-dc66-40f8-8730-489a1ab26cfb"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Hustle"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""303478dc-4733-4207-b1d8-83243c80b2e3"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Hustle"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""32852923-c7ed-472d-a1d3-f549c3f14212"",
                    ""path"": ""<Keyboard>/j"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LeftHandInteract"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""97efe1ea-e279-4d56-8f43-193f2f803f48"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LeftHandInteract"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""307199d2-c48c-441f-8a72-6ea435acede8"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LeftHandInteract"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""44445a32-a091-462a-9988-33d1d714a560"",
                    ""path"": ""<Keyboard>/k"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RightHandInteract"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""46f81163-d423-4b19-a2f3-8e68aff274b1"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Escape"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3c99b0cb-a191-4dd5-ae10-f297f774cb3f"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Escape"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // CharacterControls
        m_CharacterControls = asset.FindActionMap("CharacterControls", throwIfNotFound: true);
        m_CharacterControls_Movement = m_CharacterControls.FindAction("Movement", throwIfNotFound: true);
        m_CharacterControls_Hustle = m_CharacterControls.FindAction("Hustle", throwIfNotFound: true);
        m_CharacterControls_LeftHandInteract = m_CharacterControls.FindAction("LeftHandInteract", throwIfNotFound: true);
        m_CharacterControls_RightHandInteract = m_CharacterControls.FindAction("RightHandInteract", throwIfNotFound: true);
        m_CharacterControls_Escape = m_CharacterControls.FindAction("Escape", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // CharacterControls
    private readonly InputActionMap m_CharacterControls;
    private ICharacterControlsActions m_CharacterControlsActionsCallbackInterface;
    private readonly InputAction m_CharacterControls_Movement;
    private readonly InputAction m_CharacterControls_Hustle;
    private readonly InputAction m_CharacterControls_LeftHandInteract;
    private readonly InputAction m_CharacterControls_RightHandInteract;
    private readonly InputAction m_CharacterControls_Escape;
    public struct CharacterControlsActions
    {
        private @PlayerInput m_Wrapper;
        public CharacterControlsActions(@PlayerInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @Movement => m_Wrapper.m_CharacterControls_Movement;
        public InputAction @Hustle => m_Wrapper.m_CharacterControls_Hustle;
        public InputAction @LeftHandInteract => m_Wrapper.m_CharacterControls_LeftHandInteract;
        public InputAction @RightHandInteract => m_Wrapper.m_CharacterControls_RightHandInteract;
        public InputAction @Escape => m_Wrapper.m_CharacterControls_Escape;
        public InputActionMap Get() { return m_Wrapper.m_CharacterControls; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(CharacterControlsActions set) { return set.Get(); }
        public void SetCallbacks(ICharacterControlsActions instance)
        {
            if (m_Wrapper.m_CharacterControlsActionsCallbackInterface != null)
            {
                @Movement.started -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnMovement;
                @Movement.performed -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnMovement;
                @Movement.canceled -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnMovement;
                @Hustle.started -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnHustle;
                @Hustle.performed -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnHustle;
                @Hustle.canceled -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnHustle;
                @LeftHandInteract.started -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnLeftHandInteract;
                @LeftHandInteract.performed -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnLeftHandInteract;
                @LeftHandInteract.canceled -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnLeftHandInteract;
                @RightHandInteract.started -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnRightHandInteract;
                @RightHandInteract.performed -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnRightHandInteract;
                @RightHandInteract.canceled -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnRightHandInteract;
                @Escape.started -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnEscape;
                @Escape.performed -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnEscape;
                @Escape.canceled -= m_Wrapper.m_CharacterControlsActionsCallbackInterface.OnEscape;
            }
            m_Wrapper.m_CharacterControlsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Movement.started += instance.OnMovement;
                @Movement.performed += instance.OnMovement;
                @Movement.canceled += instance.OnMovement;
                @Hustle.started += instance.OnHustle;
                @Hustle.performed += instance.OnHustle;
                @Hustle.canceled += instance.OnHustle;
                @LeftHandInteract.started += instance.OnLeftHandInteract;
                @LeftHandInteract.performed += instance.OnLeftHandInteract;
                @LeftHandInteract.canceled += instance.OnLeftHandInteract;
                @RightHandInteract.started += instance.OnRightHandInteract;
                @RightHandInteract.performed += instance.OnRightHandInteract;
                @RightHandInteract.canceled += instance.OnRightHandInteract;
                @Escape.started += instance.OnEscape;
                @Escape.performed += instance.OnEscape;
                @Escape.canceled += instance.OnEscape;
            }
        }
    }
    public CharacterControlsActions @CharacterControls => new CharacterControlsActions(this);
    public interface ICharacterControlsActions
    {
        void OnMovement(InputAction.CallbackContext context);
        void OnHustle(InputAction.CallbackContext context);
        void OnLeftHandInteract(InputAction.CallbackContext context);
        void OnRightHandInteract(InputAction.CallbackContext context);
        void OnEscape(InputAction.CallbackContext context);
    }
}
