﻿
using UnityEngine;
using UnityEngine.UI;

public abstract class Item : MonoBehaviour
{
    public string ItemName;
    
    [TextArea] 
    public string Label;

    [SerializeField] 
    public Image InteractPrompt;

    [SerializeField] 
    public Vector2 PromptOffset = new Vector2(0, 0);

    [SerializeField] 
    public GameObject PopupImage;
    
    [SerializeField] 
    public Vector2 PopupOffset = new Vector2(-.25f, -0.5f);
    
    public abstract bool IsGrabbable { get; }

    public abstract void OnInteract(Grubber grubber);
    
    public abstract void OnDrop();

    protected virtual void ShowPopups()
    {
        Debug.Log($"Showing popups for {ItemName}");
        PopupImage.transform.position = transform.position + (Vector3) PopupOffset;
        PopupImage.SetActive(true);

        GenerateText();
    }

    protected virtual void GenerateText()
    {
        var textComponent = PopupImage.GetComponentInChildren<Text>();

        if (textComponent != null)
        {
            textComponent.text = ItemName + "\n\n" + Label;
        }
    }
    
    protected virtual void ShowPrompts()
    {
        Debug.Log($"Showing Prompts for {ItemName}");
        InteractPrompt.transform.position = transform.position + (Vector3) PromptOffset;
        InteractPrompt.gameObject.SetActive(true);
    }

    protected virtual void HidePrompts()
    { 
        Debug.Log($"Hiding Prompts for {ItemName}");
        InteractPrompt.gameObject.SetActive(false);
    }           
    
    protected virtual void HidePopups()
    {
        Debug.Log($"Hiding Popups for {ItemName}");
        PopupImage.SetActive(false);
    }

    protected abstract void HandleTriggerEnter(Grubber grubber);
    protected abstract void HandleTriggerExit(Grubber collision);

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((LayerMask.GetMask("Grubber") & 1 << collision.gameObject.layer) == 0)
        {
            return;
        }

        var grubber = collision.GetComponent<Grubber>();
        
        HandleTriggerEnter(grubber);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if ((LayerMask.GetMask("Grubber") & 1 << collision.gameObject.layer) == 0)
        {
            return;
        }
        
        var grubber = collision.GetComponent<Grubber>();
        
        HandleTriggerExit(grubber);
    }
}