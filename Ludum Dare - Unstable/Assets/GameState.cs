﻿using System;
using System.Collections;
using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameState : MonoBehaviour
{
    public Recipe CurrentRecipe;
    public Cauldron CauldronState;

    public GameObject GameOverScreen;
    
    public bool isGameStarted = false;
    public bool isGameOver = false;
    private bool inputBound;
    
    public Transform SpawnPoint;
    public Transform Waypoint;
    
    public Animator StartScreenAnim;
    public CinemachineVirtualCamera ccam;
    public Animator DialogueOverlayAnim;

    public PlayerCharacterController Player;

    public PlayerInput PlayerInput;

    private GameOverFlavor _gameOverTextField;
    private EndGameBannerText _EndGameBannerText;

    private const string EXPLOSION_FAIL = "You blew up your lab! \n You'll never get your deposit back now.";
    private const string SAMPLE_SUCCESS = "Ah, this is the correct formula!";
    private const string SAMPLE_FAIL = "Eugh, that's not quite right...";
    private const string INTRO_TEXT = "Let's see what the recipe book has for me today."; // placeholder

    public void Awake()
    {
        PlayerInput = new PlayerInput();
        
        CurrentRecipe = new Recipe();
        Player = GameObject.FindWithTag("Player").GetComponent<PlayerCharacterController>();
        
        PlayerInput.CharacterControls.LeftHandInteract.started += InterfaceInteract;
        PlayerInput.CharacterControls.Escape.started += RestartGameHandler;
        
        StartScreenAnim.gameObject.SetActive(true);

        _gameOverTextField = GameOverScreen.GetComponentInChildren<GameOverFlavor>();
        _EndGameBannerText = GameOverScreen.GetComponentInChildren<EndGameBannerText>();
    }

    public void RestartGameHandler(InputAction.CallbackContext context)
    {
        if (isGameStarted)
        {
            isGameOver = true;
            isGameStarted = false;
            SceneManager.LoadScene("SampleScene");
        }
    }
    
    public void InterfaceInteract(InputAction.CallbackContext context)
    {
        if (isGameStarted)
        {
            return;
        }
        
        if (!isGameStarted)
        {
            StartGame();
        }
        
        if (isGameOver) {
            isGameOver = false;
            SceneManager.LoadScene("SampleScene");
        }
    }
    
    public void StartGame()
    {
        ccam.Follow = Player.transform.transform;
        Player.transform.position = SpawnPoint.position;
        
        isGameStarted = true;

        StartScreenAnim.SetTrigger("FadeOutPrompt");
        
        StartCoroutine(Player.WalkToPoint(
            Waypoint.position, 
            () =>
            {
                StartScreenAnim.SetTrigger("FadeOutAll");
                Player.EnableInput();
                StartCoroutine(ShowDialogueText(INTRO_TEXT, fadeDelay: 4));
            }));
    }

    public void EndGame(bool success = false)
    {
        var flavorText = "";
        var bannerText = "";
        Color bannerColor = new Color();
        
        
        var endGameAction = new Action(() =>
        {
            _gameOverTextField.SetFlavorText(flavorText);
            _EndGameBannerText.SetEndGameBannerText(bannerText, bannerColor);
            
            GameOverScreen.SetActive(true);
            isGameOver = true;
            isGameStarted = false;
            Player.DisableInput();
        });

        if (!success)
        {
            flavorText = EXPLOSION_FAIL;
            bannerText = "GAME OVER";
            bannerColor = Color.red;
            
            StartCoroutine(CauldronState.BlowUp(endGameAction));
            return;
        }

        var finalResult = "";
        flavorText = SAMPLE_SUCCESS + finalResult;
        bannerText = "SUCCESS!";
        bannerColor = Color.green;
        endGameAction();
    }
    
    public void OnEnable()
    {
        PlayerInput.Enable();
    }

    public void OnDisable()
    {
        PlayerInput.Disable();
    }

    public void NotThereYet()
    {
        StartCoroutine(ShowDialogueText(SAMPLE_FAIL, fadeDelay: 4));
    }

    IEnumerator ShowDialogueText(string text, float fadeDelay)
    {
        Text UItext = DialogueOverlayAnim.gameObject.GetComponentInChildren<Text>();
        UItext.text = text;
        UItext.color = Color.white;
        DialogueOverlayAnim.gameObject.SetActive(true);
        DialogueOverlayAnim.SetBool("IsDisplayed", true);
        
        yield return new WaitForSeconds(fadeDelay);

        DialogueOverlayAnim.SetBool("IsDisplayed", false);
    }
}
