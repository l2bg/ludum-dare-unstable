using DefaultNamespace;
using UnityEngine;
using UnityEngine.UI;

public class Ingredient : Item
{
    public override bool IsGrabbable => true;
    
    public Vector3 AdditiveColor;

    public Vector3 SubtractiveColor;

    public StatusEffect StatusEffect;
    
    public void Setup(
        string nameString, 
        string labelString, 
        Sprite sprite, 
        Vector3 additive, 
        Vector3 subtractive, 
        StatusEffect statusEffect,
        Image interactPrompt,
        GameObject popupImage)
    {
        ItemName = nameString;
        Label = labelString;
        
        GetComponent<SpriteRenderer>().sprite = sprite;
        AdditiveColor = additive;
        SubtractiveColor = subtractive;
        StatusEffect = statusEffect;

        InteractPrompt = interactPrompt;
        PopupImage = popupImage;
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.identity;
    }

    public override void OnInteract(Grubber grubber)
    {
        Debug.Log($"GrabbedItem is: {ItemName}");
    }

    public override void OnDrop()
    {
        Debug.Log($"Dropping me: {ItemName}");
    }

    protected override void HandleTriggerEnter(Grubber grubber)
    {
        ShowPopups();
        ShowPrompts();
    }

    protected override void HandleTriggerExit(Grubber collision)
    {
        HidePopups();
        HidePrompts();
    }

    protected override void ShowPopups()
    {
        base.ShowPopups();
        Color additiveColor = new Color(AdditiveColor.x / 255, AdditiveColor.y / 255, AdditiveColor.z / 255);
        Color subtractiveColor = new Color(SubtractiveColor.x / 255, SubtractiveColor.y / 255, SubtractiveColor.z / 255);
        PopupImage.GetComponent<IngredientPopup>().SetAdditiveSwatchColor(additiveColor);
        PopupImage.GetComponent<IngredientPopup>().SetSubtractiveSwatchColor(subtractiveColor);
    }
    
    protected override void GenerateText()
    {
        var textComponent = PopupImage.GetComponentInChildren<Text>();

        if (textComponent != null)
        {
            textComponent.text = $"{ItemName}\n\n{Label}\n\n[{StatusEffect}]";
        }
    }
}
